package com.example.dima.myapplication;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

public class ShowDayTimetable extends ActionBarActivity {

    DBHelper dbHelper;
    Intent intent;
    Cursor c;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.show_day_timetable);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        intent = getIntent();

        dbHelper = new DBHelper(this);
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        TextView txtView = (TextView) findViewById(R.id.textView);

        Log.d(editTimeTable.LOG_TAG, "--- Rows in par: ---");
        // делаем запрос всех данных из таблицы par, получаем Cursor
            if (intent.getBooleanExtra("isWeekUpShow", true)) {
                c = db.query("par", null, null, null, null, null, null);
                Toast.makeText(this, "Week Up", Toast.LENGTH_SHORT).show();
                }
            else {
                c = db.query("parDown", null, null, null, null, null, null);
                Toast.makeText(this, "Week Down", Toast.LENGTH_SHORT).show();
                }

            // ставим позицию курсора на первую строку выборки
            // если в выборке нет строк, вернется false
            if (c.moveToFirst()) {

                // определяем номера столбцов по имени в выборке
                int pardayColIndex = c.getColumnIndex("parday");
                int parnameColIndex = c.getColumnIndex("parname");
                int partimeColIndex = c.getColumnIndex("partime");
                int parroomColIndex = c.getColumnIndex("parroom");
                int paritemColIndex = c.getColumnIndex("paritem");

                do {
                    if (intent.getStringExtra("Title").equals(c.getString(pardayColIndex))) {
                        txtView.setText(txtView.getText().toString() + "\n" +
                                "Name = " + c.getString(parnameColIndex) + "\n" +
                                "Time = " + c.getString(partimeColIndex) + "\n" +
                                "Room = " + c.getString(parroomColIndex) + "\n" +
                                "Item = " + c.getString(paritemColIndex) + "\n");
                    }
                } while (c.moveToNext());
            } else
                Log.d(editTimeTable.LOG_TAG, "0 rows");
            c.close();
            dbHelper.close();
    }


}
