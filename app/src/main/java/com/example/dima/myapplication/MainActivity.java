package com.example.dima.myapplication;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.graphics.*;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends ActionBarActivity implements View.OnClickListener {

    Intent intent;
    Button btnShow, btnInstall, btnSettings, btnQuit;
    public boolean isUpWeek = false;
    Dialog dialog;

      @Override
    protected void onCreate(Bundle savedInstanceState) {
          super.onCreate(savedInstanceState);
          setContentView(R.layout.activity_main);

          Animation anim = AnimationUtils.loadAnimation(this, R.anim.myalpha);
          TextView title = (TextView) findViewById(R.id.TimeTableTitle);

          btnShow = (Button) findViewById(R.id.show);
          btnInstall = (Button) findViewById(R.id.InstallBtn);
          btnSettings = (Button) findViewById(R.id.settings);
          btnQuit = (Button) findViewById(R.id.Quit);

          title.startAnimation(anim);
          btnShow.startAnimation(anim);
          btnInstall.startAnimation(anim);
          btnSettings.startAnimation(anim);
          btnQuit.startAnimation(anim);

          btnShow.setOnClickListener(this);
          btnInstall.setOnClickListener(this);
          btnSettings.setOnClickListener(this);
          btnQuit.setOnClickListener(this);

      }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.InstallBtn:
                intent = new Intent(this, editTimeTable.class);
                Dialog();
                break;
            case R.id.show:
                intent = new Intent(this, ShowTimeTable.class);
                startActivity(intent);
                break;
            case R.id.settings:
                intent = new Intent(this, Settings.class);
                startActivity(intent);
                break;
            case R.id.Quit:
                finish();
        }
    }

    public void Dialog (){
        dialog = new Dialog(MainActivity.this);
        dialog.setContentView(R.layout.custom);
        dialog.setTitle("Выберите неделю");
        ListView list = (ListView) dialog.findViewById(R.id.mylist);
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                dialog.dismiss();
                if (position == 0) {
                    isUpWeek = true;
                }
                if (position == 1) {
                    isUpWeek = false;
                }
                intent.putExtra("isUpWeek", isUpWeek);
                startActivity(intent);
            }
        });
        dialog.show();
    }
}
