package com.example.dima.myapplication;

import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.widget.Toast;

public class Settings extends ActionBarActivity implements View.OnClickListener{

    Intent intent;
    DBHelper dbhelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.settings);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        intent = getIntent();
        dbhelper = new DBHelper(this);
        (findViewById(R.id.clearTable)).setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.clearTable:
                SQLiteDatabase db = dbhelper.getWritableDatabase();
                db.delete("par", null, null);
                Toast.makeText(this, "Таблица очищена", Toast.LENGTH_SHORT).show();
                break;
        }
    }
}
