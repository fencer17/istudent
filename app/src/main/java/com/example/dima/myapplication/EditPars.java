package com.example.dima.myapplication;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.preference.EditTextPreference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceManager;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import javax.microedition.khronos.egl.EGLDisplay;

public class EditPars extends ActionBarActivity implements View.OnClickListener {

    private int DIALOG_TIME = 1;
    private int myHour = 00;
    private int myMinute = 00;
    private int ParsCount;
    private int i = 1;
    private String confirm = "Подтвердить";
    private String currentDay, TimeStr = "";
    public editTimeTable table1 = new editTimeTable();
    Intent intent;
    TextView Day_Count, txtViewTime;
    EditText edtName, edtRoom, edtItem, edtBuilding;
    Button btn_next_par, btn_previous_par;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.edit_pars);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        intent = getIntent();
        ParsCount = Integer.valueOf(intent.getStringExtra("CountPars"));
        currentDay = intent.getStringExtra("DayOfWeek");


        edtName = (EditText) findViewById(R.id.parName);
        edtRoom = (EditText) findViewById(R.id.parRoom);
        edtBuilding = (EditText) findViewById(R.id.parBuilding);
        edtItem = (EditText) findViewById(R.id.parItem);

        btn_next_par = (Button) findViewById(R.id.btnnextPar);
        btn_previous_par = (Button) findViewById(R.id.btnpreviousPar);
        txtViewTime = (TextView) findViewById(R.id.parTime);

        btn_next_par.setOnClickListener(this);
        btn_previous_par.setOnClickListener(this);
        txtViewTime.setOnClickListener(this);

        if (i == ParsCount) {
            btn_next_par.setText(confirm); // если последняя пара, то btn_next_par = "Подтвердить"
        }
        if (i == 1) {
            btn_previous_par.setEnabled(false); // если первая пара, то btn_previous_par - неактивна
        }

        Day_Count = (TextView) findViewById(R.id.DayTitle_ParCount);
        Day_Count.setText(i + " пара" + " - " + currentDay);

        if (editTimeTable.ArrayPars.size() > 0) {
            ClearAndSetText();
        }
    }

    private void edtDialog (String editFieldTxt) {
        LayoutInflater inflater = LayoutInflater.from(EditPars.this);
        View dialog_layout = inflater.inflate(R.layout.edit_text_dialog, null);
        AlertDialog.Builder dialog = new AlertDialog.Builder(EditPars.this);
        dialog.setTitle(editFieldTxt);
        dialog.setView(dialog_layout);
        dialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnnextPar:
                if (isInputDone()) {
                    if (i <= ParsCount) {
                        addItemToArray();
                        if (btn_next_par.getText().toString().equals(confirm)) {

                            for (int j = 0; j < editTimeTable.ArrayPars.size(); j++) {
                                if (editTimeTable.ArrayPars.get(j).getParIndex() > ParsCount                                       // если пар в массиве
                                        && currentDay.equals(editTimeTable.ArrayPars.get(j).getParDay().toString())) {             // больше нужного,
                                    editTimeTable.ArrayPars.remove(j);                                                             // то удалить лишние
                                }
                            }

                            if (!currentDay.equals("Суббота")) {
                                table1.i++;
                            }
                            finish();
                        } else {
                            i++;
                            ClearAndSetText();
                        }
                    }
                }
                else Toast.makeText(this, "Все поля, кроме заметки обязательны к заполнению", Toast.LENGTH_SHORT).show();
                break;

            case R.id.btnpreviousPar:
                if (isInputDone()) {
                    addItemToArray();
                }
                i--;
                ClearAndSetText();
                break;

            case R.id.parTime:
                showDialog(DIALOG_TIME);
                break;
        }

        Day_Count.setText(i + " пара" + " - " + currentDay);

        if (i == 1) {
            btn_previous_par.setEnabled(false);
        } else if (!btn_previous_par.isEnabled() && i > 1) {
            btn_previous_par.setEnabled(true);
        }
        if (i == ParsCount) {
            btn_next_par.setText(confirm);
        } else if (i < ParsCount && btn_next_par.getText().toString().equals(confirm)) {
            btn_next_par.setText("Следующая");
        }
    }




    private void ClearAndSetText () {
        edtName.setText("");
        edtRoom.setText("");
        edtItem.setText("");
        edtBuilding.setText("");
        txtViewTime.setText("");
        TimeStr = "";

        for (int j = 0; j < editTimeTable.ArrayPars.size(); j++) {
            if (i == editTimeTable.ArrayPars.get(j).getParIndex()
                    && currentDay.equals(editTimeTable.ArrayPars.get(j).getParDay().toString())) {             // элемент есть в массиве?
                TimeStr = editTimeTable.ArrayPars.get(j).getParTime().toString();
                txtViewTime.setText("Начало пары в: " + TimeStr);
                edtName.setText(editTimeTable.ArrayPars.get(j).getParName().toString());
                edtBuilding.setText(editTimeTable.ArrayPars.get(j).getParBuilding().toString());
                edtRoom.setText(editTimeTable.ArrayPars.get(j).getParRoom().toString());
                edtItem.setText(editTimeTable.ArrayPars.get(j).getParItem().toString());
                editTimeTable.ArrayPars.remove(j);
            }
        }
    }

    private boolean isInputDone () {
        if (!edtName.getText().toString().equals("") && !TimeStr.equals("") && !edtBuilding.getText().toString().equals("")
                && !edtRoom.getText().toString().equals("")) {
            return true;
        }
        else return false;
    }

    private void addItemToArray () {
            table1.addElemToArrayPar(edtName.getText().toString(), TimeStr, edtBuilding.getText().toString(),
                    edtRoom.getText().toString(), edtItem.getText().toString(), currentDay, i);
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (isInputDone()) {
            if (!btn_next_par.getText().toString().equals(confirm)) {
                addItemToArray();
            }
        }
    }

    protected Dialog onCreateDialog(int id) {
        if (id == DIALOG_TIME) {
            TimePickerDialog tpd = new TimePickerDialog(this, myCallBack, myHour, myMinute, true);
            return tpd;
        }
        return super.onCreateDialog(id);
    }

    TimePickerDialog.OnTimeSetListener myCallBack = new TimePickerDialog.OnTimeSetListener() {
        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
            myHour = hourOfDay;
            myMinute = minute;
            TimeStr = String.format("%02d:%02d", myHour, myMinute);
            txtViewTime.setText("Начало пары в: " + TimeStr);
        }
    };
}

