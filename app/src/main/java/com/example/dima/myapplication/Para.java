package com.example.dima.myapplication;

import java.util.Date;

/**
 * Created by Dima on 08.10.2015.
 */
public class Para {

    private String parName;
    private String parTime;
    private String parRoom;
    private String parBuilding;
    private String parItem;
    private String parDay;
    private Integer parIndex;



    public Para (String parNm, String parTm, String parBuilding, String parRm, String parItm, String parDy, Integer parInd) {
        setParName(parNm);
        setParTime(parTm);
        setParBuilding(parBuilding);
        setParRoom(parRm);
        setParItem(parItm);
        setParDay(parDy);
        setParIndex(parInd);
    }

    public String getParName() { return parName;  }

    public void setParName(String parName) { this.parName = parName;  }

    public String getParTime() {
        return parTime;
    }

    public void setParTime(String parTime) {
        this.parTime = parTime;
    }

    public String getParItem() {
        return parItem;
    }

    public void setParItem(String parItem) {
        this.parItem = parItem;
    }

    public String getParRoom() {
        return parRoom;
    }

    public void setParRoom(String parRoom) {
        this.parRoom = parRoom;
    }

    public String getParBuilding() {
        return parBuilding;
    }

    public void setParBuilding(String parBuilding) {
        this.parBuilding = parBuilding;
    }

    public String getParDay() {
        return parDay;
    }
    public void setParDay(String parDay) {
        this.parDay = parDay;
    }

    public Integer getParIndex() {
        return parIndex;
    }
    public void setParIndex(Integer parIndex) {
        this.parIndex = parIndex;
    }


}
