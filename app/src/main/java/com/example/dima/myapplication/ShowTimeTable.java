package com.example.dima.myapplication;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;


public class ShowTimeTable extends ActionBarActivity implements View.OnClickListener {

    Intent intent;
    boolean isWeekUp;
    private Spinner spin;
    private static int SelectedItemNumber = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.show_time_table);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        intent = new Intent(this, ShowDayTimetable.class);

        (findViewById(R.id.btnShowPon)).setOnClickListener(this);
        (findViewById(R.id.btnShowThur)).setOnClickListener(this);
        (findViewById(R.id.btnShowWen)).setOnClickListener(this);
        (findViewById(R.id.btnShowTh)).setOnClickListener(this);
        (findViewById(R.id.btnShowFri)).setOnClickListener(this);
        (findViewById(R.id.btnShowSurt)).setOnClickListener(this);

        List<String> ListOfWeeks = new ArrayList<String>();
        ListOfWeeks.add("Верхняя неделя");
        ListOfWeeks.add("Нижняя неделя");

        ArrayAdapter<String> myArrAdapter = new ArrayAdapter<String>(this, R.layout.spinner_of_the_week, ListOfWeeks);
        spin = (Spinner) findViewById(R.id.spinner);

        spin.setAdapter(myArrAdapter);

        spin.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> spin, View v, int i, long id) {
                if(spin.getSelectedItem().equals("Верхняя неделя")) {
                    isWeekUp = true;
                }
                else if (spin.getSelectedItem().equals("Нижняя неделя")) {
                    isWeekUp = false;
                }
                SelectedItemNumber = i;
                intent.putExtra("isWeekUpShow", isWeekUp);
            }

            public void onNothingSelected(AdapterView<?> parent) {
            } // empty
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        spin.setSelection(SelectedItemNumber);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnShowPon:
                intent.putExtra("Title", "Понедельник");
                break;
            case R.id.btnShowThur:
                intent.putExtra("Title", "Вторник");
                break;
            case R.id.btnShowWen:
                intent.putExtra("Title", "Среда");
                break;
            case R.id.btnShowTh:
                intent.putExtra("Title", "Четверг");
                break;
            case R.id.btnShowFri:
                intent.putExtra("Title", "Пятница");
                break;
            case R.id.btnShowSurt:
                intent.putExtra("Title", "Суббота");
                break;
            default: break;
        }
        startActivity(intent);
    }
}
