package com.example.dima.myapplication;

import android.content.ContentValues;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import java.util.ArrayList;
import java.util.LinkedList;

public class editTimeTable extends ActionBarActivity implements View.OnClickListener {

    public static final String LOG_TAG = "myLogs";

    String[] DaysOfWeek = {"Понедельник", "Вторник", "Среда", "Четверг", "Пятница", "Суббота"};
    public static int i = 0;
    public static ArrayList<Para> ArrayPars = new ArrayList<>();
    private static int[] ArrParsCount = new int[6];
    private Button nextbtn, prevbtn, apply;
    private String confirm = "Подтвердить";

    EditText parsCount;
    SQLiteDatabase db;
    ContentValues cv;
    DBHelper dbHelper;
    Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.edit_time_table);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        intent = getIntent();
        nextbtn = (Button) findViewById(R.id.btnnext);
        prevbtn = (Button) findViewById(R.id.btnprevious);
        apply = (Button) findViewById(R.id.apply);

        parsCount = (EditText) findViewById(R.id.parsQuantity);
        dbHelper = new DBHelper(this);
        cv = new ContentValues();
        db = dbHelper.getWritableDatabase();
        dbHelper.onCreate(db);

        if (i == 0) {
            prevbtn.setEnabled(false); // если понедельник, то prev - неактивен
        }
        nextbtn.setOnClickListener(this);
        prevbtn.setOnClickListener(this);
        apply.setOnClickListener(this);

    }

    @Override
    protected void onResume() {
        super.onResume();
        if (i > 0) {
            prevbtn.setEnabled(true);       // После ввода понедельника делаем кнопку prev активной
        }
        if (i == 5) {
            nextbtn.setText(confirm);       // После пятницы устанавливаем кнопку next в подтвердить
        }
        ((TextView) findViewById(R.id.DayTitle)).setText(DaysOfWeek[i]);
        onSaveParsCount();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        i=0;                              // после перехода в MainActivity сбрасываем счетчик дней недели обратно на понедельник
        ArrParsCount = new int[6];        // чистим массив количества пар
        ArrayPars.clear();                // чистим массив пар
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.btnnext:
                if (i<6) {
                    if (nextbtn.getText().toString().equals(confirm)) {
                        for(Para arpt: ArrayPars) {
                            cv.put("parday", arpt.getParDay().toString());
                            cv.put("parname", arpt.getParName().toString());
                            cv.put("partime", arpt.getParTime().toString());
                            cv.put("parroom", arpt.getParBuilding() + "/" + arpt.getParRoom().toString());
                            cv.put("paritem", arpt.getParItem().toString());

                            if (intent.getBooleanExtra("isUpWeek", true)) {
                                db.insert("par", null, cv);
                            }
                            else
                                db.insert("parDown", null, cv);
                        }
                            finish();
                        Toast.makeText(this, "Расписание сохранено", Toast.LENGTH_SHORT).show();
                            ArrayPars.clear();
                        ArrParsCount = new int[6];
                    }
                    else i++;
                    onSaveParsCount();
                }
                break;
            case R.id.btnprevious:
                if (i>0) {
                    i--;
                    onSaveParsCount();
                }
                break;
            case R.id.apply:
                try {
                    if (!parsCount.getText().toString().equals("")) {
                        if (Integer.valueOf(parsCount.getText().toString()) > 10 || Integer.valueOf(parsCount.getText().toString()) == 0) {
                            Toast.makeText(this, "Количество пар должно быть от 1 до 10", Toast.LENGTH_SHORT).show();
                        } else {
                            Intent intent = new Intent(this, EditPars.class);
                            intent.putExtra("CountPars", parsCount.getText().toString());
                            intent.putExtra("DayOfWeek", DaysOfWeek[i]);
                            ArrParsCount[i] = Integer.valueOf(parsCount.getText().toString());
                            startActivity(intent);
                        }
                    } else
                        Toast.makeText(this, "Поле не может быть пустым", Toast.LENGTH_SHORT).show();
                } catch (NumberFormatException e) {
                    Toast.makeText(this, "Слишком большое число", Toast.LENGTH_SHORT).show();
                    parsCount.setText("");
                }
        }
        ((TextView) findViewById(R.id.DayTitle)).setText(DaysOfWeek[i]);

        if (i == 0) {
            prevbtn.setEnabled(false); // если вернулись на понедельник сделать prevbtn неактивной
        }
        else if (!prevbtn.isEnabled() && i > 0) {
            prevbtn.setEnabled(true);  // при переходе на вторник сделать prevbtn активной
        }
        if (i == 5) {
            nextbtn.setText(confirm); // при переходе на субботу делаем вместо "Следующая" - "Подтвердить"
        }
        else if (i < 5 && nextbtn.getText().toString().equals(confirm)) {
            nextbtn.setText("Следующий"); // при переходе назад на пятницу делаем вместо "Подтвердить" - "Следующая"
        }
        dbHelper.close();
    }


    public void addElemToArrayPar(String parName, String parTime, String parBuilding, String parRoom, String parItem, String parDay, int parIndex) {
        Para par = new Para(parName, parTime, parBuilding, parRoom, parItem, parDay, parIndex);
        ArrayPars.add(par);
    }

    private void onSaveParsCount () {
        if (ArrParsCount[i] == 0) {
            parsCount.setText("");
        }
        else if (ArrParsCount[i] > 0) {
            parsCount.setText(String.valueOf(ArrParsCount[i]));
        }
    }
}


