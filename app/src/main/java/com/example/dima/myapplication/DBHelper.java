package com.example.dima.myapplication;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * Created by Dima on 10.10.2015.
 */
public class DBHelper extends SQLiteOpenHelper {

    public DBHelper(Context context) {
        // конструктор суперкласса
        super(context, "paraDB", null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        Log.d(editTimeTable.LOG_TAG, "--- onCreate database ---");
        // создаем таблицу с полями
        db.execSQL("CREATE TABLE IF NOT EXISTS parDown ("
                + "parday text,"
                + "parname text,"
                + "partime text,"
                + "parroom text,"
                + "paritem text" + ");");

        db.execSQL("CREATE TABLE IF NOT EXISTS par ("
                + "parday text,"
                + "parname text,"
                + "partime text,"
                + "parroom text,"
                + "paritem text" + ");");

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.d(editTimeTable.LOG_TAG, "--- onUpgrade database ---");
   //     db.execSQL("DROP DATABASE paraDB");
    }
}
